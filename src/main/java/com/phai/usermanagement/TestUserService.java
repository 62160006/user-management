/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phai.usermanagement;

/**
 *
 * @author Phai
 */
public class TestUserService {
    public static void main(String[] args) {
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("user","password"));
        System.out.println(UserService.getUsers());
        User updateUser = new User("phai","password");
        UserService.updateUser(2, updateUser);
        System.out.println(UserService.getUsers());
        UserService.delUser(updateUser);
        UserService.updateUser(0, updateUser);
    }
}
